# Fabricable Rotary Bearings

## CAD is here

[Fab Cross Roller Bearing](/cad/Fab_CRB.f3d) follows directly on Laimer.
[Fab BLDC](/cad/Fab_BLDC.f3d) contains a bearing that uses balls, not rollers, in a cross-type. 

## What it is

Largely following on [Christoph Laimer](http://www.laimer.ch/)

What we want is a large diameter bearing that will handle some big off-axis loads. In 'industry' we use Cross Roller Slew Bearings:

![crb](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/nice-slew.jpg)

These rollers are crowned, and caged, such that they maintain their orientation. If you look here:

![crb](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/slew-singlecontact-evidence.JPG)

We can see that the rollers actually have only one contacting point. The rollers are slightly barrel-shaped. 

![wb with sam](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/whiteboard-with-sam.jpg)

The issue I am having with [Christoph Laimer's](http://www.laimer.ch/) slew bearings is that the rollers tend to want to track out of the race, which causes their edges to grind on the non-rotating side. 

Here are Laimer's bearings:

![laimer-fusion](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/bearings-cross-roller-fusion.jpg)

![laimer-fab](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/bearings-cross-roller.jpg)

Here's a [really interesting link to an explanation of how crowned pulleys self-right](https://woodgears.ca/bandsaw/crowned_pulleys.html). 

Etc, the solution I like is to use spherical rollers. You can find [Delrin Balls](https://www.mcmaster.com/#balls-made-with-delrin-acetal-resin/=1akk64u) on McMaster that are +/- 0.001" in diameter - these seem to work really well. 

## CAD

![das bearing](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/fabbearing.png)

In Fusion I draw a four-sectioned bearing with race walls that are of a *just slightly* larger radius than that of the balls. I like to use a radius that will also let me mill the race with a ball end mill.

TODO: make nicely parametric, release fusion doc

## CAM

![doing-the-milling](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/doing-the-milling.gif)

![cam-doozy](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/cam-doozy.jpg)

The CAM is a bit tricky, as you need to tell the ball endmill to track around the center of the race, which involves drawing that center line in a sketch (rather than selecting the race geometry) and building a 'trace' toolpath with that. However, it works well! 

Last tricks: (1) take a *very* shallow final pass, so that you don't get any chatter and bumpiness in the surface. Should be super ultra smooth etc. (2) Face your stock! It's very important that the distance between the race and the face (haha) is keps in the same WCS, so we can't rely on the stock height.

## Assembly

When I put these together, I tend to assemble the inner races, and lay them flat with one of the outer races hooped around. Then I can drop balls in one by one:

![cam-doozy](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/assembly.jpg)

Then locking the outer ring on is no problem.

Critically, there's some preloading that goes on in this step. Ideally we would design the depth of the races such that when the faces are cinched together we would have the *perfect* preloading condition, but in reality a +/- 0.0005" error can make a big difference in preload - so you'll probably spend some time un-tightening / tightening screws until it feels right. With some care, these can feel really good! And you get to pick whatever size you want, they cost almost nothing, and they're light as heck!

## OK

~ fin ~

![one bearing](https://gitlab.cba.mit.edu/jakeread/fabbearing/raw/master/images/bearing-complete.gif)